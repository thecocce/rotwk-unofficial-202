# Modern UI example script
!include MUI.nsh
!include sections.nsh

Name "2.02 German Language Pack"
OutFile "202_v7_german_language.exe"
InstallDirRegKey HKLM "Software\Electronic Arts\Electronic Arts\The Lord of the Rings, The Rise of the Witch-king" InstallPath

SetCompressor /SOLID lzma

; Customization of the license page (so it says readme rather than license)
;!define MUI_PAGE_HEADER_TEXT "Read Me"
;!define MUI_PAGE_HEADER_SUBTEXT "For new players"
;!define MUI_LICENSEPAGE_TEXT_TOP "Please read the following information:"
;!define MUI_LICENSEPAGE_TEXT_BOTTOM " . "
;!define MUI_LICENSEPAGE_BUTTON "Next >"

; Customization of the finish page (checkbox for desktop schortcut)
;!define MUI_FINISHPAGE_SHOWREADME ""
;!define MUI_FINISHPAGE_SHOWREADME_NOTCHECKED
;!define MUI_FINISHPAGE_SHOWREADME_TEXT "Create Desktop Shortcut"
;!define MUI_FINISHPAGE_SHOWREADME_FUNCTION finishpageaction

;!define MUI_ICON "202icon.ico"

!insertmacro MUI_PAGE_WELCOME
;!insertmacro MUI_PAGE_LICENSE "license.rtf"
;!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_LANGUAGE "English"
;!insertmacro MUI_LANGUAGE "German"
;!insertmacro MUI_LANGUAGE "German"

BrandingText "Rise of the Witch-King 2.02 -- www.gamereplays.org"


;;
; Main component
;;
Section
	;SectionIn RO ;Make it read-only
	SetOutPath $INSTDIR

	File /r "German/*"

	;~ File /r v7std\lang
	;~ File /r v7std\launcher_releases

	;~ writeUninstaller "$INSTDIR\uninstall02German.exe"

SectionEnd



