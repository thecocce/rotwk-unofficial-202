/*
 * Release.h
 *
 * Copyright 2017 Bram Bosch <bagbosch@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#pragma once

// For console output
#include <QTextStream>

// For file handling
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QVector>

// For md5sum
#include <QCryptographicHash>

#include "SwitchFile.h"

class Release 
{
	public:
		Release(QString newName);
		Release();

		QString getName();
		QString getEnableFile(int fileID);
		void addEnableFile(QString toAdd);
		void addDisableFile(QString toAdd);
		void addEnableFileChecksum(QString toAdd);
		void addSwitchFile(SwitchFile toAdd);
		void addTriggeredRelease(Release toAdd);
		QString applyRelease();
		QString integrityCheckRelease();

	private:
		//Attributes
		QString name;
		QVector<QString> enableFiles;
		QVector<QString> enableFileChecksums;
		QVector<QString> disableFiles;
		QVector<SwitchFile> switchFiles;
		
		QVector<Release> triggeredReleases;

		//Methods
		bool enableFile(QString fileName);
		bool disableFile(QString fileName);
		bool integrityCheckFile(int fileID);
		SwitchFile getSwitchFile(int fileID);
		QString fileSHA1(QString file_name);
};
