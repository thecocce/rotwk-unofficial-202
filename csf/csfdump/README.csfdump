csfdump
=======

A program to extract strings from CSF files used in EA games.


Usage:

  csfdump <file>

The file may optionally be compressed by a certain compression that EA
uses for some of the files inside their BIG files and binary streams;
csfdump automatically attempts to decompress a file that is not a
straight CSF file.

The output is of the form

  LABEL;{text};{extra text}

LABEL is an ASCII string, text and extra text are output as UTF-8, and
may indeed be any character from the Unicode Basic Multilingual Plane.

I have never encountered "extra text" in real-life CSF files, but
apparently there is an option for a string item to contain additional
text beyond the first string.


Compilation:

This should compile with any ISO-C++ conforming compiler, for example with GCC use

  g++ -O2 -s -o csfdump.exe csfdump.cpp decompress.cpp


Known issues:

Error handling and reporting is minimal. Probably the program does not
deal well with mal-formed input files. Some minimal protection is
built in, limiting string sizes to 4kB, but prepared files can
probably still wreak havoc. If you're worried, put in some sanity
checks.


License:

Feel free to use and reuse, but only if the same license applies to
your resulting product.
