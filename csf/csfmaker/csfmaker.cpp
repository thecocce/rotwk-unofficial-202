#include <cassert>
#include <cstdint>
#include <iostream>
#include <fstream>
#include <vector>

const char csf_file_id[4] = {'C','S','F',' '};
const char csf_label_id[4] = {'L','B','L',' '};
const char csf_string_id[4] = {'S','T','R',' '};
const char csf_string_w_id[4] = {'S','T','R','W'};

struct t_csf_header
{
  char id[4];
  char flags1[4];
  char count1[4];
  char count2[4];
  char zero[4];
  char flags2[4];
};

void write_char4(const char * in, char * out)
{ (out)[3] = (in)[0]; (out)[2] = (in)[1]; (out)[1] = (in)[2]; (out)[0] = (in)[3]; }

void write_int_to_char(unsigned int in, char * out)
{ out[0] = (in & 0x000000FF); out[1] = (in & 0x0000FF00) >> 8; out[2] = (in & 0x00FF0000) >> 16; out[3] = (in & 0xFF000000) >> 24; }

std::vector<uint32_t> decode(const uint8_t * s);

struct Result
{
  Result(std::string l, std::string c) : label(l), content(c) { }
  std::string label;
  std::string content;
};

int main(int argc, char * argv[])
{

  std::istream &  in = std::cin;
  std::ofstream out("MyOutFile.csf", std::ios::binary);

  union { char c; unsigned char u; } B;

  enum Modes { LABELMODE = 100, CONTENTMODE = 200 } mode = LABELMODE;

  enum SpecialChars { FIELDSEP = ';', OPENC = '{', CLOSEC = '}' };

  std::string label, content;
  std::vector<Result> results;

  int bubble(0);   // we allow delimiters as literals, as long as they're perfectly matched!

  while(!in.eof())
  {
    in.read(&B.c, 1);

    if (mode == LABELMODE && B.c != FIELDSEP && B.c != OPENC)
    {
      label.append(&B.c, 1);
      continue;
    }
    else if ((mode == LABELMODE) && (B.c == char(FIELDSEP)))
    {
      continue;
    }
    else if ((mode == LABELMODE) && (B.c == char(OPENC)))
    {
      mode = CONTENTMODE;
      continue;
    }
    else if ((mode == CONTENTMODE) && (B.c == CLOSEC))
    {
      if (bubble) { bubble--; content.append(&B.c, 1); continue; }

      mode = LABELMODE;

      results.push_back(Result(label, content));

      label.clear();
      content.clear();
      //std::cerr << "Having cleared, we have " << label.length() << " " << content.length() << std::endl;

      // Gobble newline
      char n;
      if(!in.eof())
      {
        in.read(&n, 1);
        if (n != 10 && n != 13)
        {
          in.putback(n);
        }
        else if (n == 10)
        {
          continue;
        }
        else if (n == 13 && !in.eof())
        {
          in.read(&n, 1);
          if (n != 10) in.putback(n);
        }
      }
      continue;
    }
    else if ((mode == CONTENTMODE) && (B.c != CLOSEC))
    {
      content.append(&B.c, 1);
      if (B.c == OPENC) bubble++;
      continue;
    }
    else
    {
      std::cerr << "Warning: Should never get here!" << std::endl;
    }
  }

  std::cerr << "We read " << results.size() << " pairs." << std::endl;
  std::cerr << "Fieldsep = " << char(FIELDSEP) << ", Open = " << char(OPENC)
            << ", Close = " << char(CLOSEC) << "." << std::endl;
  //  for (std::vector<Result>::const_iterator i = results.begin(); i != results.end(); i++) std::cerr << i->.label << ";{" << i->content << "}" << std::endl;

  t_csf_header header;

  write_char4(csf_file_id, header.id);
  write_int_to_char(results.size(), header.count1);
  write_int_to_char(results.size(), header.count2);
  write_int_to_char(0, header.flags1);
  write_int_to_char(0, header.flags2);
  write_int_to_char(0, header.zero);

  out.write(reinterpret_cast<char *>(&header), sizeof(header));
  for (int i = 0; i < results.size(); ++i)
  {
    //out << "Entry " << i << ": Label \"" << results[i].label << "\", length = " << results[i].label.length() << ", content length = " << results[i].content.length() << std::endl;

    char buf[4];

    // "LBL "
    write_char4(csf_label_id, buf);
    out.write(buf, 4);

    // "1", flag saying "do read string"
    write_int_to_char(1, buf);
    out.write(buf, 4);

    // size of label string
    write_int_to_char(results[i].label.length(), buf);
    out.write(buf, 4);

    // the string itself
    out.write(results[i].label.c_str(), results[i].label.length());

    // "STR "
    write_char4(csf_string_id, buf);
    out.write(buf, 4);

    // UTF8-to-Codepoint converter
    std::vector<uint32_t> u = decode(reinterpret_cast<const uint8_t *>(results[i].content.c_str()));

    // size of content string
    write_int_to_char(u.size(), buf);
    out.write(buf, 4);

    // content string
    for (int i = 0; i < u.size(); ++i)
    {
      write_int_to_char(u[i], buf);
      buf[0] = ~buf[0]; buf[1] = ~buf[1];
      assert(buf[2] == 0 && buf[3] == 0);
      out.write( reinterpret_cast<char *>(&(buf[0])), 1);
      out.write( reinterpret_cast<char *>(&(buf[1])), 1);
    }
  }

  return 0;
  
}
